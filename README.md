# Homework 10

Homework 10 for CSE 30332 - Programming Paradigms, based on our Microwave times API and numbersapi.com

## Microwave Server API

| Request Type | Resource Endpoint | Body | Response | Description |
| --- | --- | --- | --- | --- |
| GET | /microwave/:food/:start_temp/:wattage | N/A | \{"result": "success", "time": x\} | Get time for specific food |
| GET | /microwave/reviews | N/A | \{"result": success, "reviews": \[\{"pasta": \{"good": x, "bad": y\}\}, ….\]\} | Get list of reviews |
| GET | /microwave/reviews/:food | N/A | \{"result": "success", "good": x, "bad": y\} | Get reviews for specific food |
| POST | /microwave/reviews | \{"food": "pasta", "review": "good"\} | \{"result": "success"} | Post a review |
## Testing

##### API

Test the server and API by running `python3 src/test_api.py`  
This will run unit tests for each of the different API calls


## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.
