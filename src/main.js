console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('microwave-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var food = document.getElementById("food-input").value;
    console.log('Food you entered is ' + food);
    makeNetworkCallToMicrowaveApi(food);

} // end of get form info

function makeNetworkCallToMicrowaveApi(food){
    console.log('entered make nw call ' + food);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://localhost:51048/microwave/" + food + "/frozen/1100";
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateResultWithResponse(food, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null)

} // end of make nw call

function updateResultWithResponse(food, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line1");

    if(response_json['time'] == null){
        label1.innerHTML = 'We don\'t know that food!'
    } else{
        var time = parseFloat(response_json['time']);
        time = Math.round(time * 100) / 100
        label1.innerHTML =  'You should microwave ' + food + ' for ' + time + ' seconds.'
        makeNetworkCallToRandomDoggie(food);
    }
} // end of updateAgeWithResponse

function makeNetworkCallToRandomDoggie(food){
    console.log('enetered makeNetworkCallToRandomDoggie');
    var url = "https://dog.ceo/api/breeds/image/random";
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        console.log('got nw response from dogsapi');
        updateImageWithResponse(food, xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null); // send request without bosy
} // end of makeNetworkCallToRandomDoggie

function updateImageWithResponse(food, response_text){
    var image = document.getElementById("doggie-pic");

    var label2 = document.getElementById("response-line2");
    label2.innerHTML =  "This dog loves " + food + "!";

    // dynamically adding a label
    var response_json = JSON.parse(response_text)
    image.src = response_json['message'];
    image.alt = "This dog loves " + food + "!";
} // end of updateImageWithResponse
