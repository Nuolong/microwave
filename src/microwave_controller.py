# Controller class with event handlers for dictionary requests

import json
import cherrypy
import copy, random
import reviews

class MicrowaveController(object):

    # Load Food/Time Data
    def __init__(self):
        with open('../data/times.json') as f:
            self.data = json.loads(f.read())

    # Implement
    def GET_TIME_KEY(self, food, start_temp, wattage):
        output_json = {'result': 'success'}

        try:
            output_json['time'] = self.data['food'][food] * self.data['wattage_ratios'][wattage] * self.data['temp'][start_temp]
        except KeyError:
            output_json['result'] = 'error'
        except Exception as ex:
            output_json['result'] = 'error'
            output_json['message'] = str(ex)

        return json.dumps(output_json)

    # TODO: Implement
    def GET_REVIEW_ALL(self):
        output_json = {'result': 'success', 'reviews': []}
        rev_data = reviews.open_reviews()

        try:
            for food in rev_data:
                output_json['reviews'].append({food: {'good': rev_data[food]['good'], 'bad': rev_data[food]['bad']}})
        except Exception as ex:
            output_json['result'] = 'error'
            output_json['message'] = str(ex)
            print(str(ex))

        return json.dumps(output_json)

    # TODO: Implement
    def GET_REVIEW_KEY(self, food):
        output_json = {'result': 'success'}
        rev_data = reviews.open_reviews()

        try:
            output_json['good'], output_json['bad'] = reviews.get_food_reviews(rev_data, food)
        except KeyError:
            output_json['result'] = 'error'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output_json)

    # TODO: Implement
    def POST_REVIEW_INDEX(self):
        output_json = {'result': 'success'}

        rev_data = reviews.open_reviews()
        rev_str = cherrypy.request.body.read()

        if not rev_str:
            output['result'] = 'error'
            output['message'] = 'no body specified'
            return json.dumps(output_json)

        rev_json = json.loads(rev_str)

        print(rev_data, rev_json['food'], rev_json['review'])
        reviews.add_food_review(rev_data, rev_json['food'], rev_json['review']);
        reviews.close_reviews(rev_data)

        return json.dumps(output_json)
