# OO Library for reviews data

import json

# Create reviews json dictionary from file
def open_reviews():
    with open('../data/reviews.json', 'r') as f:
        rev_data = json.loads(f.read())
    return rev_data

# Return tuple of reviews by type (good, bad) for food
def get_food_reviews(rev_data, food):
    if food not in rev_data:
        rev_data[food] = {}
        rev_data[food]['good'] = 0
        rev_data[food]['bad']  = 0
        return 0, 0

    if 'good' not in rev_data[food] or 'bad' not in rev_data[food]:
        rev_data[food]['good'] = 0
        rev_data[food]['bad']  = 0
        return 0, 0

    return rev_data[food]['good'], rev_data[food]['bad']

# Add a good or bad review for food
def add_food_review(rev_data, food, review):
    if food not in rev_data:
        rev_data[food] = {}
        rev_data[food]['good'] = 0
        rev_data[food]['bad']  = 0

    rev_data[food][review] += 1

# Write updated reviews json
def close_reviews(rev_data):
    with open('../data/reviews.json', 'w') as fw:
        fw.write(json.dumps(rev_data))


